<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>It's That Easy</title>
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="css/material.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script defer src="js/material.min.js"></script> 
</head>
<body>
	<div class="mdl-layout mdl-js-layout">
		<header class="mdl-layout__header" style="background-color: white;">
			<div class="mdl-layout__header-row">
				<span class="mdl-layout-title"><a href="index.php"><img src="img/its_that_easy_logo.png" class="logo"></a></span>
				<div class="mdl-layout-spacer"></div>
			</div>
		</header>
		<div class="mdl-layout__drawer">
    		<nav class="mdl-navigation">
    			<!-- <span style="color: white; font-size: 20px;margin-left: 600px;cursor: pointer;">X</span> -->
		      <a class="mdl-navigation__link" href="index.php">HOME</a>
		      <a class="mdl-navigation__link" href="service.php">SERVICE</a>
		      <a class="mdl-navigation__link" href="about.php">ABOUT US</a>
		      <a class="mdl-navigation__link" href="counseller.php">COUNSELOR / THERAPIST</a>
		      <a class="mdl-navigation__link" href="faq.php">FAQ's</a>
		      <!-- <a class="mdl-navigation__link" href="">BLOGS</a> -->
		      <a class="mdl-navigation__link" href="contact.php">CONTACT US</a>
    		</nav>
  		</div>	
	</div>
	<div class="mdl-layout__content">
		<div class="its-head">
			<center>
				<h4>Is Your Marriage in Question and you are facing  real dilemma Unable to decide that, should you walk out or should you stay.</h4>
				<p>Let us make that easy for you. Take the following self-analysis test for you which will help you in understanding your stand and find your way.</p>
				<a class="mdl-button mdl-js-button mdl-button--raised its-head1" href="1.php">Take The Test <i class="material-icons">arrow_forward</i></a>
			</center>
		</div>
		<div class="fixed-action-btn" style="bottom: 45px; right: 35px;">
			<a href="contact.php" class="btn-floating btn-large" style="color: black;">
				SKIP THIS TEST
			</a>
		</div>
	</div>
</body>
</html>