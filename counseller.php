<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>It's That Easy || Counseler / Therapist</title>
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="css/material.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script defer src="js/material.min.js"></script> 
</head>
<body>
	<div class="mdl-layout mdl-js-layout">
		<header class="mdl-layout__header" style="background-color: white;">
			<div class="mdl-layout__header-row">
				<span class="mdl-layout-title"><a href="index.php"><img src="img/its_that_easy_logo.png" class="logo"></a></span>
				<div class="mdl-layout-spacer"></div>
			</div>
		</header>
		<div class="mdl-layout__drawer">
    		<nav class="mdl-navigation">
    			<!-- <span style="color: white; font-size: 20px;margin-left: 600px;cursor: pointer;">X</span> -->
		      <a class="mdl-navigation__link" href="index.php">HOME</a>
		      <a class="mdl-navigation__link" href="service.php">SERVICE</a>
		      <a class="mdl-navigation__link" href="about.php">ABOUT US</a>
		      <a class="mdl-navigation__link" href="counseller.php">COUNSELOR / THERAPIST</a>
		      <a class="mdl-navigation__link" href="faq.php">FAQ's</a>
		      <!-- <a class="mdl-navigation__link" href="">BLOGS</a> -->
		      <a class="mdl-navigation__link" href="contact.php">CONTACT US</a>
    		</nav>
  		</div>	
	</div>
	<div class="mdl-layout__container">
		<div class="test-container" style="color: black;">
			<div class="test-content">
				<a href="index.php">
					<i class="material-icons">arrow_backword</i>
				</a>
			</div>
			<div class="couns-section" >
				<h3>Counselor / Therapist</h3>
			</div>
			<div class="mdl-grid portfolio-max-width">
				<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet mdl-card mdl-shadow--4dp">
					<div class="mdl-card__media mdl-cell mdl-cell--12-col-tablet">
						<img src="img/img1.jpg" class="article-image">
					</div>
					<div class="mdl-cell mdl-cell--8-col">
						<h3 class="mdl-card__title-text">Lorem Ipsum</h3>
						<div class="mdl-card__supporting-text padding-top">
							<span>therapist</span>
						</div>
						<div class="mdl-card__supporting-text no-left-padding">
							<p>Excepteur reprehenderit sint exercitation ipsum consequat qui sit id velit elit. Velit anim eiusmod labore sit amet. Voluptate voluptate irure occaecat deserunt incididunt esse in. Qui ullamco consectetur aute fugiat officia ullamco proident Lorem ad irure. Sint eu ut consectetur ut esse veniam laboris adipisicing aliquip minim anim labore commodo.</p>
						</div>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet mdl-card mdl-shadow--4dp">
					<div class="mdl-card__media mdl-cell mdl-cell--12-col-tablet">
						<img src="img/img1.jpg" class="article-image">
					</div>
					<div class="mdl-cell mdl-cell--8-col">
						<h3 class="mdl-card__title-text">Lorem Ipsum</h3>
						<div class="mdl-card__supporting-text padding-top">
							<span>therapist</span>
						</div>
						<div class="mdl-card__supporting-text no-left-padding">
							<p>Excepteur reprehenderit sint exercitation ipsum consequat qui sit id velit elit. Velit anim eiusmod labore sit amet. Voluptate voluptate irure occaecat deserunt incididunt esse in. Qui ullamco consectetur aute fugiat officia ullamco proident Lorem ad irure. Sint eu ut consectetur ut esse veniam laboris adipisicing aliquip minim anim labore commodo.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="mdl-mega-footer">
		<div class="mdl-mega-footer__middle-section">
			<ul class="mdl-mega-footer--link-list">
				<center>
					<li>
					<!-- <input type="email" name="email" id="mail" class="sub-form"> -->
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    					<input type="email" class="mdl-textfield__input" id="subs"/>
                    				<label class="mdl-textfield__label" for="subs">Subscribe for newsletter</label>
                    					<button class="mdl-button mdl-js-button mdl-button--raised form-button subs-button" for="subs">Subscribe</button>
                					</div>
				</li>
				<li class="footer"><p style="font-size: 14px; margin-top: 5px;">MADE BY <span style="color: red;">&#9829</span> WITH <a href="#"> TEAM VARIANCE</a></p>
				</li>
				</center>
			</ul>
		</div>
	</footer>
	</div>
</body>
</html>