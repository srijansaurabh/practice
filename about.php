<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>It's That Easy || Contact Us</title>
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="css/material.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script defer src="js/material.min.js"></script> 
</head>
<body>
	<div class="mdl-layout mdl-js-layout">
		<header class="mdl-layout__header" style="background-color: white;">
			<div class="mdl-layout__header-row">
				<span class="mdl-layout-title"><a href="index.php"><img src="img/its_that_easy_logo.png" class="logo"></a></span>
				<div class="mdl-layout-spacer"></div>
			</div>
		</header>
		<div class="mdl-layout__drawer">
    		<nav class="mdl-navigation">
    			<!-- <span style="color: white; font-size: 20px;margin-left: 600px;cursor: pointer;">X</span> -->
		      <a class="mdl-navigation__link" href="index.php">HOME</a>
		      <a class="mdl-navigation__link" href="service.php">SERVICE</a>
		      <a class="mdl-navigation__link" href="about.php">ABOUT US</a>
		      <a class="mdl-navigation__link" href="counseller.php">COUNSELOR / THERAPIST</a>
		      <a class="mdl-navigation__link" href="faq.php">FAQ's</a>
		      <!-- <a class="mdl-navigation__link" href="">BLOGS</a> -->
		      <a class="mdl-navigation__link" href="contact.php">CONTACT US</a>
    		</nav>
  		</div>	
	</div>
	<div class="mdl-layout__container">
		<div class="test-container about-section" style="color: black;">
			<div class="test-content">
				<a href="index.php">
					<i class="material-icons">arrow_backword</i>
				</a>
			</div>
			<div class="" style="margin-bottom: 20px;">
				<div class="contact-page">
					<h3>About Us</h3>
				</div>
				<div class="mdl-grid portfolio-max-width">
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet ">
						<div>
							<p style="font-size: 18px;text-align: center;">It's that Easy is a platform and organization to help couples who seek counseling for their relationship.
							We understand how stressful this phase might be for you. Our trusted Legal counselors and therapist ensure to help you in the easiest way. 
							Our experts try best to give you warm, safe, empathetic and a non-judgmental environment.
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="mdl-mega-footer">
		<div class="mdl-mega-footer__middle-section">
			<ul class="mdl-mega-footer--link-list">
				<center>
					<li>
					<!-- <input type="email" name="email" id="mail" class="sub-form"> -->
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    					<input type="email" class="mdl-textfield__input" id="subs"/>
                    				<label class="mdl-textfield__label" for="subs">Subscribe for newsletter</label>
                    					<button class="mdl-button mdl-js-button mdl-button--raised form-button subs-button" for="subs">Subscribe</button>
                					</div>
				</li>
				<li class="footer"><p style="font-size: 14px; margin-top: 5px;">MADE BY <span style="color: red;">&#9829</span> WITH <a href="#"> TEAM VARIANCE</a></p>
				</li>
				</center>
			</ul>
		</div>
	</footer>
	</div>
</body>
</html>