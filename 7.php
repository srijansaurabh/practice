<?php 
if (isset($_POST['six'])) { 
$presecond=$_POST['presecond'];
$prefirst=$_POST['prefirst'];
$prethird = $_POST['prethird'];
$prefour = $_POST['prefour'];
$prefive = $_POST['prefive'];
$six = $_POST['six'];
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>It's That Easy</title>
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="css/material.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script defer src="js/material.min.js"></script> 
</head>
<body>
	<div class="mdl-layout mdl-js-layout">
		<header class="mdl-layout__header" style="background-color: white;">
			<div class="mdl-layout__header-row">
				<span class="mdl-layout-title"><a href="index.php"><img src="img/its_that_easy_logo.png" class="logo"></a></span>
				<div class="mdl-layout-spacer"></div>
			</div>
		</header>
		<div class="mdl-layout__drawer">
    		<nav class="mdl-navigation">
    			<!-- <span style="color: white; font-size: 20px;margin-left: 600px;cursor: pointer;">X</span> -->
		      <a class="mdl-navigation__link" href="index.php">HOME</a>
		      <a class="mdl-navigation__link" href="service.php">SERVICE</a>
		      <a class="mdl-navigation__link" href="about.php">ABOUT US</a>
		      <a class="mdl-navigation__link" href="counseller.php">COUNSELOR / THERAPIST</a>
		      <a class="mdl-navigation__link" href="faq.php">FAQ's</a>
		      <!-- <a class="mdl-navigation__link" href="">BLOGS</a> -->
		      <a class="mdl-navigation__link" href="contact.php">CONTACT US</a>
    		</nav>
  		</div>	
	</div>
	<div class="mdl-layout__container">
		<div class="test-container" style="color: black;">
			<div class="test-content">
				<a href="6.php">
					<i class="material-icons">arrow_backword</i>
				</a>
			</div>
			<div class="mdl-grid test-content1">
				<div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet">
					<img src="img/7.png" style="" class="seven">
				</div>
				<div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet ques-content">
					<h4>What kind of agreement do you want?</h4>
					<ul class="">
						<li onclick="sevena(this)">
							<span class="opt-size seven-opt">A</span>
							<span class="span-opt seven-ans">
								<p>Protect your rights only</p>
								<p>Are only good for you</p>
								<p>Give your spouse less</p>
								<p>Do not inconvenience you</p>
								<p>Need frequent court hearings to enforce</p>	
							</span>
						</li>
						<li onclick="sevenb(this)">
							<span class="opt-size seven-opt">B</span>
							<span class="span-opt seven-ans">
								<p>Respect your spouse's rights too</p>
								<p>Are good for everyone</p>
								<p>Give your spouse what is rightfully theirs</p>
								<p>Work well for everyone</p>
								<p>Need no court hearings to enforce</p>	
							</span>
						</li>
					</ul>
					<form action="8.php" method="POST" id="myForm">
						<input type="hidden" name="seven" id="seven">
						<input type="hidden" name="presecond" value="<?php echo $presecond; ?>">
						<input type="hidden" name="prefirst" value="<?php echo $prefirst; ?>">
						<input type="hidden" name="prethird" value="<?php echo $prethird; ?>">
						<input type="hidden" name="prefour" value="<?php echo $prefour; ?>">
						<input type="hidden" name="prefive" value="<?php echo $prefive; ?>">
						<input type="hidden" name="presix" value="<?php echo $six; ?>">
					</form>
				</div>
			</div>
			<center>
				<div class="progress-bar">
					<span class="progress-tab-disable">1</span>
					<span class="progress-tab-disable">2</span>
					<span class="progress-tab-disable">3</span>
					<span class="progress-tab-disable">4</span>
					<span class="progress-tab-disable">5</span>
					<span class="progress-tab-disable">6</span>
					<span class="progress-tab">7</span>
					<span class="progress-tab-disable">8</span>
				</div>
			</center>
		</div>
	</div>
</body>
</html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">
	function sevena(el){
		$('#seven').val('divorce7');
		var myForm = document.getElementById("myForm");	
		myForm.submit();
	}
	function sevenb(el){
		$('#seven').val('live7');
		var myForm = document.getElementById("myForm");
		myForm.submit();
	}
	function sevenc(el){
		$('#seven').val('other7');
		var myForm = document.getElementById("myForm");
		myForm.submit();
	}
</script>