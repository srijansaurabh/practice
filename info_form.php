<?php
if (isset($_POST['eight'])) { 
	$presecond=$_POST['presecond'];
	$prefirst=$_POST['prefirst'];
	$prethird = $_POST['prethird'];
	$prefour = $_POST['prefour'];
	$prefive = $_POST['prefive'];
	$presix = $_POST['presix'];
	$preseven = $_POST['preseven'];
	$eight = $_POST['eight'];
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>It's That Easy</title>
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="css/material.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script defer src="js/material.min.js"></script> 
</head>
<body>
	<div class="mdl-layout mdl-js-layout">
		<header class="mdl-layout__header" style="background-color: white;">
			<div class="mdl-layout__header-row">
				<span class="mdl-layout-title"><a href="index.php"><img src="img/its_that_easy_logo.png" class="logo"></a></span>
				<div class="mdl-layout-spacer"></div>
			</div>
		</header>
		<div class="mdl-layout__drawer">
    		<nav class="mdl-navigation">
    			<!-- <span style="color: white; font-size: 20px;margin-left: 600px;cursor: pointer;">X</span> -->
		      <a class="mdl-navigation__link" href="index.php">HOME</a>
		      <a class="mdl-navigation__link" href="service.php">SERVICE</a>
		      <a class="mdl-navigation__link" href="about.php">ABOUT US</a>
		      <a class="mdl-navigation__link" href="counseller.php">COUNSELOR / THERAPIST</a>
		      <a class="mdl-navigation__link" href="faq.php">FAQ's</a>
		      <!-- <a class="mdl-navigation__link" href="">BLOGS</a> -->
		      <a class="mdl-navigation__link" href="contact.php">CONTACT US</a>
    		</nav>
  		</div>	
	</div>
	<div class="mdl-layout__container">
		<div class="test-container" style="color: black;">
			<div class="test-content">
				<a href="8.php">
					<i class="material-icons">arrow_backword</i>
				</a>
			</div>
			<div class="mdl-grid form-container">
				<div class="mdl-cell mdl-cell--12-col mdl-card form-content">
					<h4>Help Us</h4>
					<form action="class.phpmail.php" method="post">
						<div class="mdl-textfield mdl-js-textfield">
    						<input class="mdl-textfield__input" type="text" id="sample1" name="name">
   							<label class="mdl-textfield__label" for="sample1">Full name</label>
  						</div>
  						<div class="mdl-textfield mdl-js-textfield">
    						<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="sample2" name="mob_no">
    						<label class="mdl-textfield__label" for="sample2">Mobile Number...</label>
    						<span class="mdl-textfield__error">Input is not a number!</span>
  						</div>
  						<div class="mdl-textfield mdl-js-textfield">
    						<input class="mdl-textfield__input" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" id="sample3" name="email">
    						<label class="mdl-textfield__label" for="sample3">Email...</label>
    						<span class="mdl-textfield__error">Input is not a email!</span>
  						</div>
  						<div class="mdl-textfield mdl-js-textfield">
    						<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="sample4" name="marraige">
    						<label class="mdl-textfield__label" for="sample4">Years of marriage...</label>
    						<span class="mdl-textfield__error">Input in number please</span>
  						</div>
  						<div class="mdl-textfield mdl-js-textfield">
    						<input class="mdl-textfield__input" type="text" id="sample5" name="city">
   							<label class="mdl-textfield__label" for="sample5">City</label>
  						</div>
  						<div>
  							<button class="mdl-button mdl-js-button mdl-button--raised form-button" name="submit">Submit</button>
  						</div>
  						<input type="hidden" name="presecond" value="<?php echo $presecond; ?>">
						<input type="hidden" name="prefirst" value="<?php echo $prefirst; ?>">
						<input type="hidden" name="prethird" value="<?php echo $prethird; ?>">
						<input type="hidden" name="prefour" value="<?php echo $prefour; ?>">
						<input type="hidden" name="prefive" value="<?php echo $prefive; ?>">
						<input type="hidden" name="presix" value="<?php echo $presix; ?>">
						<input type="hidden" name="preseven" value="<?php echo $preseven; ?>">
						<input type="hidden" name="preeight" value="<?php echo $eight; ?>">
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>