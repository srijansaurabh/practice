<?php 
if (isset($_POST['five'])) { 
$presecond = $_POST['presecond'];
$prefirst = $_POST['prefirst'];
$prethird = $_POST['prethird'];
$prefour = $_POST['prefour'];
$five = $_POST['five'];
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>It's That Easy</title>
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="css/material.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script defer src="js/material.min.js"></script> 
</head>
<body>
	<div class="mdl-layout mdl-js-layout">
		<header class="mdl-layout__header" style="background-color: white;">
			<div class="mdl-layout__header-row">
				<span class="mdl-layout-title"><a href="index.php"><img src="img/its_that_easy_logo.png" class="logo"></a></span>
				<div class="mdl-layout-spacer"></div>
			</div>
		</header>
		<div class="mdl-layout__drawer">
    		<nav class="mdl-navigation">
    			<!-- <span style="color: white; font-size: 20px;margin-left: 600px;cursor: pointer;">X</span> -->
		      <a class="mdl-navigation__link" href="index.php">HOME</a>
		      <a class="mdl-navigation__link" href="service.php">SERVICE</a>
		      <a class="mdl-navigation__link" href="about.php">ABOUT US</a>
		      <a class="mdl-navigation__link" href="counseller.php">COUNSELOR / THERAPIST</a>
		      <a class="mdl-navigation__link" href="faq.php">FAQ's</a>
		      <!-- <a class="mdl-navigation__link" href="">BLOGS</a> -->
		      <a class="mdl-navigation__link" href="contact.php">CONTACT US</a>
    		</nav>
  		</div>	
	</div>
	<div class="mdl-layout__container">
		<div class="test-container" style="color: black;">
			<div class="test-content">
				<a href="5.php">
					<i class="material-icons">arrow_backword</i>
				</a>
			</div>
			<div class="mdl-grid test-content1">
				<div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet">
					<img src="img/6.png" style="">
				</div>
				<div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet ques-content">
					<h4>Can you handle the unpleasant consequences of divorce ?</h4>
					<ul class="">
						<li onclick="sixa(this)">
							<span class="opt-size">A</span><span class="span-opt">Yes</span>
						</li>
						<li onclick="sixb(this)">
							<span class="opt-size">B</span><span class="span-opt">No</span>
						</li>
						<li onclick="sixc(this)">
							<span class="opt-size">C</span><span class="span-opt">Please help me understand the consequences</span>
						</li>
					</ul>
					<form action="7.php" method="POST" id="myForm">
						<input type="hidden" name="six" id="six">
						<input type="hidden" name="presecond" value="<?php echo $presecond; ?>">
						<input type="hidden" name="prefirst" value="<?php echo $prefirst; ?>">
						<input type="hidden" name="prethird" value="<?php echo $prethird; ?>">
						<input type="hidden" name="prefour" value="<?php echo $prefour; ?>">
						<input type="hidden" name="prefive" value="<?php echo $five; ?>">
					</form>
				</div>
			</div>
			<center>
				<div class="progress-bar">
					<span class="progress-tab-disable">1</span>
					<span class="progress-tab-disable">2</span>
					<span class="progress-tab-disable">3</span>
					<span class="progress-tab-disable">4</span>
					<span class="progress-tab-disable">5</span>
					<span class="progress-tab">6</span>
					<span class="progress-tab-disable">7</span>
					<span class="progress-tab-disable">8</span>
				</div>
			</center>
		</div>
	</div>
</body>
</html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">
	function sixa(el){
		$('#six').val('yes');
		var myForm = document.getElementById("myForm");	
		myForm.submit();
	}
	function sixb(el){
		$('#six').val('no');
		var myForm = document.getElementById("myForm");
		myForm.submit();
	}
	function sixc(el){
		$('#six').val('please help me understand the consequences');
		var myForm = document.getElementById("myForm");
		myForm.submit();
	}
</script>