<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>It's That Easy || Contact Us</title>
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="css/material.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script defer src="js/material.min.js"></script> 
</head>
<body>
	<div class="mdl-layout mdl-js-layout">
		<header class="mdl-layout__header" style="background-color: white;">
			<div class="mdl-layout__header-row">
				<span class="mdl-layout-title"><a href="index.php"><img src="img/its_that_easy_logo.png" class="logo"></a></span>
				<div class="mdl-layout-spacer"></div>
			</div>
		</header>
		<div class="mdl-layout__drawer">
    		<nav class="mdl-navigation">
    			<!-- <span style="color: white; font-size: 20px;margin-left: 600px;cursor: pointer;">X</span> -->
		      <a class="mdl-navigation__link" href="index.php">HOME</a>
		      <a class="mdl-navigation__link" href="service.php">SERVICE</a>
		      <a class="mdl-navigation__link" href="about.php">ABOUT US</a>
		      <a class="mdl-navigation__link" href="counseller.php">COUNSELOR / THERAPIST</a>
		      <a class="mdl-navigation__link" href="faq.php">FAQ's</a>
		      <!-- <a class="mdl-navigation__link" href="">BLOGS</a> -->
		      <a class="mdl-navigation__link" href="contact.php">CONTACT US</a>
    		</nav>
  		</div>	
	</div>
	<div class="mdl-layout__container">
		<div class="test-container" style="color: black;">
			<div class="test-content">
				<a href="index.php">
					<i class="material-icons">arrow_backword</i>
				</a>
			</div>
			<div class="" style="margin-bottom: 20px;">
				<div class="contact-page">
					<h3>Contact Us</h3>
				</div>
				<div class="mdl-grid portfolio-max-width">
					<form accept="" method="">
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-card mdl-shadow--4dp">
						<div class="mdl-grid mdl-cell--6-col mdl-cell--4-col-tablet">
								<div class="mdl-grid">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    					<input type="text" class="mdl-textfield__input" id="username"/>
                    					<label class="mdl-textfield__label" for="username">Username</label>
                					</div>
								</div>
								<div class="mdl-grid">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    					<input type="number" class="mdl-textfield__input" id="age"/>
                    					<label class="mdl-textfield__label" for="age">Age</label>
                					</div>
								</div>
								<div class="mdl-grid">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    					<input type="text" class="mdl-textfield__input" id="gender"/>
                    					<label class="mdl-textfield__label" for="gender">Gender</label>
                					</div>
								</div>
								<div class="mdl-grid">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    					<input type="text" class="mdl-textfield__input" id="yom"/>
                    					<label class="mdl-textfield__label" for="yom">Years of Marriage</label>
                					</div>
								</div>
						</div>
						<div class="mdl-grid mdl-cell--6-col mdl-cell--4-col-tablet">
							<div class="mdl-grid">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    				<input type="email" class="mdl-textfield__input" id="email"/>
                    				<label class="mdl-textfield__label" for="email">Email</label>
                				</div>
							</div>
							<div class="mdl-grid">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    				<input type="number" class="mdl-textfield__input" id="mobile"/>
                    				<label class="mdl-textfield__label" for="mobile">Mobile No</label>
                				</div>
							</div>
							<div class="mdl-grid">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    				<input type="text" class="mdl-textfield__input" id="city"/>
                    				<label class="mdl-textfield__label" for="city">City</label>
                				</div>
							</div>
							<div class="mdl-grid">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<label class="contact-label">Children: </label>
                    				<label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
  										<input type="radio" id="option-1" class="mdl-radio__button" name="options" value="1" checked>
  										<span class="mdl-radio__label">Yes</span>
									</label>
									<label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
  										<input type="radio" id="option-2" class="mdl-radio__button" name="options" value="2">
  										<span class="mdl-radio__label">No</span>
									</label>
                				</div>
							</div>
						</div>
						<div class="mdl-grid">
							<button class="mdl-button mdl-js-button mdl-button--raised form-button" name="submit">Submit</button>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
		<footer class="mdl-mega-footer">
		<div class="mdl-mega-footer__middle-section">
			<ul class="mdl-mega-footer--link-list">
				<center>
					<li>
					<!-- <input type="email" name="email" id="mail" class="sub-form"> -->
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    					<input type="email" class="mdl-textfield__input" id="subs"/>
                    				<label class="mdl-textfield__label" for="subs">Subscribe for newsletter</label>
                    					<button class="mdl-button mdl-js-button mdl-button--raised form-button subs-button" for="subs">Subscribe</button>
                					</div>
				</li>
				<li class="footer"><p style="font-size: 14px; margin-top: 5px;">MADE BY <span style="color: red;">&#9829</span> WITH <a href="#"> TEAM VARIANCE</a></p>
				</li>
				</center>
			</ul>
		</div>
	</footer>
	</div>
</body>
</html>