<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>It's That Easy || Faq's</title>
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="css/material.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script defer src="js/material.min.js"></script> 
</head>
<body>
	<div class="mdl-layout mdl-js-layout">
		<header class="mdl-layout__header" style="background-color: white;">
			<div class="mdl-layout__header-row">
				<span class="mdl-layout-title"><a href="index.php"><img src="img/its_that_easy_logo.png" class="logo"></a></span>
				<div class="mdl-layout-spacer"></div>
			</div>
		</header>
		<div class="mdl-layout__drawer">
    		<nav class="mdl-navigation">
    			<!-- <span style="color: white; font-size: 20px;margin-left: 600px;cursor: pointer;">X</span> -->
		      <a class="mdl-navigation__link" href="index.php">HOME</a>
		      <a class="mdl-navigation__link" href="service.php">SERVICE</a>
		      <a class="mdl-navigation__link" href="about.php">ABOUT US</a>
		      <a class="mdl-navigation__link" href="counseller.php">COUNSELOR / THERAPIST</a>
		      <a class="mdl-navigation__link" href="faq.php">FAQ's</a>
		      <!-- <a class="mdl-navigation__link" href="">BLOGS</a> -->
		      <a class="mdl-navigation__link" href="contact.php">CONTACT US</a>
    		</nav>
  		</div>	
	</div>
	<div class="mdl-layout__container">
		<div>
			<div class="test-content">
				<a href="index.php">
					<i class="material-icons">arrow_backword</i>
				</a>
			</div>
			<div class = "mdl-tabs mdl-js-tabs">
               <div class = "mdl-tabs__tab-bar">
                  <a href = "#tab1-panel" class = "mdl-tabs__tab is-active"><b>FAQ's Divorce</b></a>
                  <a href = "#tab2-panel" class = "mdl-tabs__tab"><b>FAQ's Counseling</b></a>
               </div>
            
               <div class = "mdl-tabs__panel is-active" id = "tab1-panel">
                  <div class="faq-tab">
                  	<ul>
                  		<li>
                  			<h5>- What is mutual divorce ?</h5>
                  			<p>A mutual divorce is one in which there is both parties agree to the divorce and the terms of the settlement of all issues (property and debt division, alimony, and child custody and support).</p>
                  		</li>
                  		<li>
                  			<h5>- How long does it take to get a divorce?</h5>
                  			<p>A minimum period of six months is stipulated under the law for a motion for passing decree of divorce on the basis of mutual consent.</p>
                  		</li>
                  		<li>
                  			<h5>- Is there any way to waive off the six months period?</h5>
                  			<p>Yes, the six months period can be waived off by the Court under certain special circumstances.</p>
                  		</li>
                  		<li>
                  			<h5>- I am unable to personally appear before the Court. Is there still a way that I can get a mutual divorce?</h5>
                  			<p>Yes, Courts can also use the medium of video conferencing and permit representation through close relations such as parents or siblings under special circumstances.</p>
                  		</li>
                  		<li>
                  			<h5>- How is child custody determined?</h5>
                  			<p>In a mutual divorce child custody is determined mutually by the parties. Typically one parent retains the custody of the child and the other takes visitation rights. This entirely depends on the arrangement between the parties. The Custodial parent makes the day to day decisions regarding the children whereas the non-custodial parent is allotted certain times to have visitation with the children. The modern trend is to try to keep both parents active in the lives of their children, which has led to the concept of joint custody, and, to try to put both parents on a somewhat equal footing.</p>
                  		</li>
                  		<li>
                  			<h5>- How is property divided?</h5>
                  			<p>Consideration is often given to how and when the property was acquired, favoring that each party will be able to keep property he or she owned before the marriage, or acquired by gift or inheritance during the marriage. The underlying concept is that property should be divided fairly.</p>
                  		</li>
                  		<li>
                  			<h5>- How is alimony/maintenance determined?</h5>
                  			<p>When a couple gets divorced by mutual consent, the decision on whether any alimony/maintenance is to be paid by either party is a matter of agreement between them.</p>
                  		</li>
                  		<li>
                  			<h5>- What is a contested divorce?</h5>
                  			<p>Under contested divorce, either of the spouses can file a divorce petition. Some grounds are given in law which needs to be proved by the party who is seeking a divorce.</p>
                  		</li>
                  		<li>
                  			<h5>- What is annulment of marriage?</h5>
                  			<p>arriages in India may also be ended by annulment. Annulment results in the status of the couple to alter back to what it was prior to the marriage, i.e, it would be as if the marriage had never taken place.</p>
                  		</li>
                  		<li>
                  			<h5>- What is judicial separation?</h5>
                  			<p>Judicial separation means a provision where both the spouses under a decree from the court, live separately for a specified period, getting enough time to think on the decision of divorce. In judicial separation, the husband and wife do not live with each other, i.e. cohabitation rights between the couple ceases but the marital status remains intact.</p>
                  		</li>
                  	</ul>
                  </div>
               </div>
            
               <div class = "mdl-tabs__panel" id = "tab2-panel">
                  <div class="faq-tab">
                  	<ul>
                  		<li>
                  			<h5>- Do we come in together or separately?</h5>
                  			<p>Beginning the therapy together gives you both a chance to tell your story. Often the first session will give both of you hope that you can work on the relationship and make a difference.</p>
                  		</li>
                  		<li>
                  			<h5>- What can we expect from couples therapy?</h5>
                  			<p>Sessions are generally 45-min long but we can have longer sessions if required. The 1st session is an assessment where our therapists gather background information from you and your partner. The 2nd and 3rd sessions we usually meet with partners individually to understand your relationship histories. Starting the 4th session, we bring couples back together again to track your interaction patterns.</p>
                  		</li>
                  		<li>
                  			<h5>- Is your practice religion based?</h5>
                  			<p>Religion is not incorporated in sessions unless requested by the couple. With that said, we do have traditional values in promoting strong marriages and we do believe in working to keep a couple together when possible.</p>
                  		</li>
                  		<li>
                  			<h5>- Is there any point in coming if we’ve already filed for divorce?</h5>
                  			<p>Yes, couples who have filed for divorce are able to reconcile. Even if you do not wish to reconcile, you can gain clarity about why your relationship has reached this point so that you can both make a conscious choice and be better prepared for future relationships. Finally, if you have children, it is your duty to relate to each other in a healthy way as you will continue to be in relationship as parents to your children.</p>
                  		</li>
                  		<li>
                  			<h5>- Do you offer online sessions?</h5>
                  			<p>Yes, we do offer online sessions over skype and other mutually convenient platforms.</p>
                  		</li>
                  		<li>
                  			<h5>- Do you see children?</h5>
                  			<p>Yes we do see children provided that both parents or the parent having custody gives us a no-objection.</p>
                  		</li>
                  		<li>
                  			<h5>- What happens in a session?</h5>
                  			<p>During sessions, we help you recognize the type of painful and ineffective negative cycle that causes you to get stuck in an argument with you partner and leaves you feeling angry, hurt, rejected and alone. During our sessions, we will address the fundamental need for connection and the fear of losing each other.</p>
                  		</li>
                  		<li>
                  			<h5>- What if we fight during the session?</h5>
                  			<p>When you work with us, we won’t let you fight in our sessions. We put all of our effort into tuning in and truly listening to both of you.</p>
                  		</li>
                  		<li>
                  			<h5>- We have been in couples therapy before, we felt like the therapist was taking sides. What if that happens again?</h5>
                  			<p>If one of you starts to feel that we are taking sides, we are not doing our job well. We work really hard so that both of you feel that we do understand your experiences. However, couples therapy is a tricky business, and if you ever feel that we are taking sides, the sooner we know about that, the sooner we can take action. This is your treatment and we need your feedback to serve you better.</p>
                  		</li>
                  		<li>
                  			<h5>- What is your success rate?</h5>
                  			<p>Our success rate is 90%. We measure our marriage counseling success rate based on the number of couples that we’ve seen in our counseling practice that have met their intended goals for entering into counseling.</p>
                  		</li>
                  	</ul>
                  </div>
               </div>
            </div>
		</div>
      <footer class="mdl-mega-footer">
      <div class="mdl-mega-footer__middle-section">
         <ul class="mdl-mega-footer--link-list">
            <center>
               <li>
               <!-- <input type="email" name="email" id="mail" class="sub-form"> -->
               <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                 <input type="email" class="mdl-textfield__input" id="subs"/>
                              <label class="mdl-textfield__label" for="subs">Subscribe for newsletter</label>
                                 <button class="mdl-button mdl-js-button mdl-button--raised form-button subs-button" for="subs">Subscribe</button>
                              </div>
            </li>
            <li class="footer"><p style="font-size: 14px; margin-top: 5px;">MADE BY <span style="color: red;">&#9829</span> WITH <a href="#"> TEAM VARIANCE</a></p>
            </li>
            </center>
         </ul>
      </div>
   </footer>
	</div>
</body>
</html>