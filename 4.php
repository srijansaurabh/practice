<?php 
if (isset($_POST['third'])) { 
$presecond=$_POST['presecond'];
$prefirst=$_POST['prefirst'];
$third = $_POST['third'];

}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>It's That Easy</title>
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="css/material.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script defer src="js/material.min.js"></script> 
</head>
<body>
	<div class="mdl-layout mdl-js-layout">
		<header class="mdl-layout__header" style="background-color: white;">
			<div class="mdl-layout__header-row">
				<span class="mdl-layout-title"><a href="index.php"><img src="img/its_that_easy_logo.png" class="logo"></a></span>
				<div class="mdl-layout-spacer"></div>
			</div>
		</header>
		<div class="mdl-layout__drawer">
    		<nav class="mdl-navigation">
    			<!-- <span style="color: white; font-size: 20px;margin-left: 600px;cursor: pointer;">X</span> -->
		      <a class="mdl-navigation__link" href="index.php">HOME</a>
		      <a class="mdl-navigation__link" href="service.php">SERVICE</a>
		      <a class="mdl-navigation__link" href="about.php">ABOUT US</a>
		      <a class="mdl-navigation__link" href="counseller.php">COUNSELOR / THERAPIST</a>
		      <a class="mdl-navigation__link" href="faq.php">FAQ's</a>
		      <!-- <a class="mdl-navigation__link" href="">BLOGS</a> -->
		      <a class="mdl-navigation__link" href="contact.php">CONTACT US</a>
    		</nav>
  		</div>	
	</div>
	<div class="mdl-layout__container">
		<div class="test-container" style="color: black;">
			<div class="test-content">
				<a href="3.php">
					<i class="material-icons">arrow_backword</i>
				</a>
			</div>
			<div class="mdl-grid test-content1">
				<div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet">
					<img src="img/4.png" style="">
				</div>
				<div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet ques-content">
					<h4>Have you made attempt to fix the relationship/situation ?</h4>
					<ul class="">
						<li onclick="foura(this)">
							<span class="opt-size">A</span><span class="span-opt">Yes</span>
						</li>
						<li onclick="fourb(this)">
							<span class="opt-size">B</span><span class="span-opt">No</span>
						</li>
						<li onclick="fourc(this)">
							<span class="opt-size">C</span><span class="span-opt">Please Help me fix the situation</span>
						</li>
					</ul>
					<form action="5.php" method="POST" id="myForm">
						<input type="hidden" name="four" id="four">
						<input type="hidden" name="presecond" value="<?php echo $presecond; ?>">
						<input type="hidden" name="prefirst" value="<?php echo $prefirst; ?>">
						<input type="hidden" name="prethird" value="<?php echo $third; ?>">

					</form>
				</div>
			</div>
			<center>
				<div class="progress-bar">
					<span class="progress-tab-disable">1</span>
					<span class="progress-tab-disable">2</span>
					<span class="progress-tab-disable">3</span>
					<span class="progress-tab">4</span>
					<span class="progress-tab-disable">5</span>
					<span class="progress-tab-disable">6</span>
					<span class="progress-tab-disable">7</span>
					<span class="progress-tab-disable">8</span>
				</div>
			</center>
		</div>
	</div>
</body>
</html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">
	function foura(el){
		$('#four').val('yes');
		var myForm = document.getElementById("myForm");	
		myForm.submit();
	}
	function fourb(el){
		$('#four').val('no');
		var myForm = document.getElementById("myForm");
		myForm.submit();
	}
	function fourc(el){
		$('#four').val('please help me fix the situation');
		var myForm = document.getElementById("myForm");
		myForm.submit();
	}
</script>